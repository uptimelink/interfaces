import { IDomainSnapshot } from '../data/domainsnapshot.js';
import { ILinkSnapshot } from '../data/linksnapshot.js';
import * as plugins from '../ul-interfaces.plugins.js';

export interface IReq_PerformDomainSnapshot
  extends plugins.typedRequestInterfaces.implementsTR<
    plugins.typedRequestInterfaces.ITypedRequest,
    IReq_PerformDomainSnapshot
  > {
  method: 'performDomainSnapshot';
  request: {
    domainName: string;
  };
  response: {
    domainSnapshot: IDomainSnapshot;
    linkSnapshot: ILinkSnapshot;
  };
}
