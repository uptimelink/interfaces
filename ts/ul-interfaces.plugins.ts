import * as typedRequestInterfaces from '@apiglobal/typedrequest-interfaces';

export { typedRequestInterfaces };

// tsclass scope
import * as tsclass from '@tsclass/tsclass';

export { tsclass };
