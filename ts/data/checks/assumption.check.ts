import * as plugins from '../../ul-interfaces.plugins.js';
import { TCheckResultStatus, TExecutionTiming } from './index.js';

export interface IAssumptionCheck {
  inputData: {
    domain: string;
    title?: string;
    statusCode?: string;
    description?: string;
    dnsRecords?: plugins.tsclass.network.IDnsRecord;
  };
  executionResults: Array<{
    timing: TExecutionTiming;
    status: TCheckResultStatus;
    data: {
      domain: string;
      title?: string;
      statusCode?: string;
      description?: string;
      dnsRecords: Array<plugins.tsclass.network.IDnsRecord>;
    };
  }>;
}
