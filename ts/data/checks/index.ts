export type TCheckResultStatus = 'ok' | 'not ok' | 'timed out';

export interface TExecutionTiming {
  plannedTime: number;
  timeStarted: number;
  timeEnded: number;
  duration: number;
}

export * from './assumption.check.js';
export * from './function.check.js';
export * from './pagerank.check.js';
export * from './pwa.check.js';
